package com.example.valorantapp.data.api

import com.example.valorantapp.data.model.Data
import com.example.valorantapp.data.model.Weapon
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiInterface {

    @GET("weapons")
    fun getWeapons(): Call<Data>

    @GET("weapons/{uuid}")
    fun getCurrentWeapon(@Path("uuid") uuid: String): Call<Data>

    companion object {
        val BASE_URL = "https://valorant-api.com/v1/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

}