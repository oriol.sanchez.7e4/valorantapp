package com.example.valorantapp.data.model

data class DamageRange(
    val bodyDamage: Double,
    val headDamage: Double,
    val legDamage: Double,
    val rangeEndMeters: Int,
    val rangeStartMeters: Int
)