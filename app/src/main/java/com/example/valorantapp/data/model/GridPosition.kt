package com.example.valorantapp.data.model

data class GridPosition(
    val column: Int,
    val row: Int
)