package com.example.valorantapp.data.model

data class Level(
    val assetPath: String,
    val displayIcon: String,
    val displayName: String,
    val levelItem: Any,
    val streamedVideo: String,
    val uuid: String
)