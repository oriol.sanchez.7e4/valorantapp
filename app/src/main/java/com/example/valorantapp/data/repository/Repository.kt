package com.example.valorantapp.data.repository

import com.example.valorantapp.data.api.ApiInterface

class Repository {
    private val apiService = ApiInterface.create()
    fun getWeapons() = apiService.getWeapons()
    fun getCurrentWeapon(id: String) = apiService.getCurrentWeapon(id)
}