package com.example.valorantapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.valorantapp.database.dao.WeaponDao
import com.example.valorantapp.database.entity.WeaponEntity

@Database(entities = [WeaponEntity::class], version = 2)
abstract class Database: RoomDatabase() {
    abstract fun weaponDao(): WeaponDao
}
