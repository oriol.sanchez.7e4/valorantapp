package com.example.valorantapp.database

import android.app.Application
import androidx.room.Room

class WeaponApplication: Application() {
    companion object {
        lateinit var database: Database
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            Database::class.java,
            "Database")
            .fallbackToDestructiveMigration()
            .build()
    }
}
