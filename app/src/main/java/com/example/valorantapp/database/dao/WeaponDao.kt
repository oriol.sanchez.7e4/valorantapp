package com.example.valorantapp.database.dao

import androidx.room.*
import com.example.valorantapp.database.entity.WeaponEntity

@Dao
interface WeaponDao {
    @Query("SELECT * FROM WeaponEntity")
    fun getAllWeapons(): MutableList<WeaponEntity>
    @Insert
    fun addWeapons(weaponEntity: WeaponEntity)
    @Delete
    fun deleteWeapon(weaponEntity: WeaponEntity)

}