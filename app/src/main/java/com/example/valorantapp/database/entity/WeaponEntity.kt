package com.example.valorantapp.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "WeaponEntity")
data class WeaponEntity(
    @PrimaryKey var uuid: String
    )
