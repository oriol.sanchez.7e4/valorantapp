package com.example.valorantapp.ui.main.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapp.R
import com.example.valorantapp.data.model.DamageRange
import com.example.valorantapp.data.model.Data
import com.example.valorantapp.data.model.Weapon
import com.example.valorantapp.ui.main.view.WeaponsFragmentDirections
import com.example.valorantapp.ui.main.viewModel.WeaponsViewModel
import com.squareup.picasso.Picasso

class DamageAdapter(private var viewModel: WeaponsViewModel) : RecyclerView.Adapter<DamageAdapter.DamageListViewHolder>() {
    var damages = mutableListOf<DamageRange>()

    class DamageListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var range_start: TextView = itemView.findViewById(R.id.range_start)
        private var range_end: TextView = itemView.findViewById(R.id.range_end)
        private var head_dmg: TextView = itemView.findViewById(R.id.head_damage)
        private var body_dmg: TextView = itemView.findViewById(R.id.body_damage)
        private var leg_dmg: TextView = itemView.findViewById(R.id.leg_damage)

        @SuppressLint("SetTextI18n")
        fun bindData(damage: DamageRange) {
            range_start.text = damage.rangeStartMeters.toString()
            range_end.text = damage.rangeEndMeters.toString()
            damage.headDamage.toFloat()
            head_dmg.text = "Head Damage - " + String.format("%.2f", damage.headDamage)
            body_dmg.text = "Body Damage - " + String.format("%.2f", damage.bodyDamage)
            leg_dmg.text = "Leg Damage - " + String.format("%.2f", damage.legDamage)
        }
    }

    /**
     * gets the damages from the currentWeapon, and puts it on the damages mutableListOf
     */
    fun setDamageList(damageList: List<DamageRange>) {
        if(damages.isEmpty()){
            for (i in damageList){
                damages.add(i)
            }
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DamageListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_damage, parent, false)
        return DamageListViewHolder(view)
    }

    override fun onBindViewHolder(holder: DamageListViewHolder, position: Int) {
        holder.bindData(damages[position])
    }

    override fun getItemCount(): Int {
        return damages.size
    }


}