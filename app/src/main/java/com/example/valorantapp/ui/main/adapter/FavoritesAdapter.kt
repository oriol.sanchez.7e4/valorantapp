package com.example.valorantapp.ui.main.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapp.R
import com.example.valorantapp.data.model.Data
import com.example.valorantapp.data.model.Weapon
import com.example.valorantapp.database.dao.WeaponDao
import com.example.valorantapp.database.entity.WeaponEntity
import com.example.valorantapp.ui.main.view.FavoritesFragmentDirections
import com.example.valorantapp.ui.main.view.WeaponsFragmentDirections
import com.example.valorantapp.ui.main.viewModel.WeaponsViewModel
import com.squareup.picasso.Picasso

class FavoritesAdapter(viewModel: WeaponsViewModel) : RecyclerView.Adapter<FavoritesAdapter.WeaponListViewHolder>() {
    var weapons = mutableListOf<Weapon>()
    private var viewModel = viewModel
    var weaponsFav = mutableListOf<Weapon>()

    class WeaponListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var displayName: TextView = itemView.findViewById(R.id.displayName)
        private var imgWeapon: ImageView = itemView.findViewById(R.id.imgWeapon)

        fun bindData(weapon: Weapon) {
            val url_img = weapon.displayIcon
            displayName.text = weapon.displayName
            Picasso.get().load(url_img).into(imgWeapon)
        }
    }

    /**
     * Puts the Favorites Weapons in the weaponsFav mutableListOf
     */
    fun setFavList(weaponList: Data) {
        if(weaponsFav.isEmpty()){
            for (i in weaponList.data){
                weaponsFav.add(i)
            }
        }
        notifyDataSetChanged()
    }

    /**
     * Compares uuid's from each list and puts the Weapon to the FavList, to show on the FavoritesFragment
     */
    fun setFavoriteList(weaponList: MutableList<WeaponEntity>){
        for (i in weaponList){
            for (j in weapons){
                if (i.uuid==j.uuid){
                    j.favorite = true
                    Log.w("SEARCH", ""+j)
                    viewModel.favorites.add(j)
                    weaponsFav.add(j)
                }
            }
        }
        Log.w("weaponList",""+weaponList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeaponListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_weaponitem, parent, false)
        return WeaponListViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeaponListViewHolder, position: Int) {
        holder.bindData(weaponsFav[position])

        holder.itemView.setOnClickListener {
            viewModel.weaponDetailed = weaponsFav[position]

            Navigation.findNavController(holder.itemView).navigate(FavoritesFragmentDirections.actionFavoritesFragmentToDetailWeaponFragment())
        }
    }

    override fun getItemCount(): Int {
        return weaponsFav.size
    }

}