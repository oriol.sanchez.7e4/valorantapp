package com.example.valorantapp.ui.main.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapp.R
import com.example.valorantapp.data.model.Data
import com.example.valorantapp.data.model.Weapon
import com.example.valorantapp.database.dao.WeaponDao
import com.example.valorantapp.database.entity.WeaponEntity
import com.example.valorantapp.ui.main.view.WeaponsFragmentDirections
import com.example.valorantapp.ui.main.viewModel.WeaponsViewModel
import com.squareup.picasso.Picasso

class WeaponsAdapter(viewModel: WeaponsViewModel) : RecyclerView.Adapter<WeaponsAdapter.WeaponListViewHolder>() {
    var weapons = mutableListOf<Weapon>()
    private var viewModel = viewModel
    var weaponFav = mutableListOf<Weapon>()

    class WeaponListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var displayName: TextView = itemView.findViewById(R.id.displayName)
        private var imgWeapon: ImageView = itemView.findViewById(R.id.imgWeapon)

        fun bindData(weapon: Weapon) {
            val url_img = weapon.displayIcon
            displayName.text = weapon.displayName
            Picasso.get().load(url_img).into(imgWeapon)
        }
    }

    fun setWeaponList(weaponList: Data) {
        if(weapons.isEmpty()){
            for (i in weaponList.data){
                weapons.add(i)
            }
        }
        notifyDataSetChanged()
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeaponListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_weaponitem, parent, false)
        return WeaponListViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeaponListViewHolder, position: Int) {
        holder.bindData(weapons[position])

        holder.itemView.setOnClickListener {
            viewModel.weaponDetailed = weapons[position]

            Navigation.findNavController(holder.itemView).navigate(WeaponsFragmentDirections.weaponsFragmentToDetailWeaponFragment())
        }
    }

    override fun getItemCount(): Int {
        return weapons.size
    }

    fun setFavoriteList(weaponList: MutableList<WeaponEntity>){
        viewModel.favorites.removeAll(viewModel.favorites)
        for (i in weaponList){
            for (j in weapons){
                if (i.uuid==j.uuid){
                    j.favorite = true
                    viewModel.favorites.add(j)
                    weaponFav.add(j)
                }
            }
        }
        Log.w("weaponList",""+weaponList)
        notifyDataSetChanged()
    }

}