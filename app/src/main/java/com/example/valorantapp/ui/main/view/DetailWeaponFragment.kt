package com.example.valorantapp.ui.main.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapp.R
import com.example.valorantapp.data.model.Weapon
import com.example.valorantapp.ui.main.adapter.DamageAdapter
import com.example.valorantapp.ui.main.adapter.WeaponsAdapter
import com.example.valorantapp.ui.main.viewModel.WeaponsViewModel
import com.squareup.picasso.Picasso
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.valorantapp.database.WeaponApplication
import com.example.valorantapp.database.entity.WeaponEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class DetailWeaponFragment: Fragment(R.layout.fragment_detail_weapon) {
    private lateinit var displayName: TextView
    private lateinit var imageView: ImageView
    private lateinit var price: TextView
    private lateinit var sizeMagazine: TextView
    private lateinit var timeReload: TextView
    private lateinit var category: TextView
    private lateinit var recyclerView: RecyclerView
    private lateinit var favoriteToggle: ImageButton

    private val viewModel: WeaponsViewModel by activityViewModels()
    lateinit var currentWeapon: Weapon

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        currentWeapon = viewModel.weaponDetailed
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favoriteToggle = view.findViewById(R.id.favoriteToggle)
        if (currentWeapon.favorite){
            favoriteToggle.setImageResource(R.drawable.favorit)

        } else {
            favoriteToggle.setImageResource(R.drawable.favorit_empty)
        }

        recyclerView = view.findViewById(R.id.recycler_damages)
        val adapter = DamageAdapter(viewModel)

        if (currentWeapon.uuid != "2f59173c-4bed-b6c3-2191-dea9b58be9c7"){

            recyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            recyclerView.adapter = adapter

            adapter.setDamageList(currentWeapon.weaponStats.damageRanges)
        }

        displayName = view.findViewById(R.id.displayName)
        imageView = view.findViewById(R.id.imageView)
        price = view.findViewById(R.id.price)
        sizeMagazine = view.findViewById(R.id.sizeMagazine)
        timeReload = view.findViewById(R.id.timeReload)
        category = view.findViewById(R.id.category)

        var url_img = currentWeapon.displayIcon
        displayName.text = currentWeapon.displayName
        Picasso.get().load(url_img).into(imageView)

        if (!currentWeapon.uuid.equals("2f59173c-4bed-b6c3-2191-dea9b58be9c7")){
            price.text = "Price: " + currentWeapon.shopData.cost
            sizeMagazine.text = currentWeapon.weaponStats.magazineSize.toString() + " balas"
            category.text = currentWeapon.shopData.category
            timeReload.text = currentWeapon.weaponStats.reloadTimeSeconds.toString() + " segundos"
        } else{
            price.text = "Price: " + "FREE"
            sizeMagazine.text = "Sin balas"
            category.text = "Melee"
            timeReload.text = "0 segundos"
        }

        favoriteToggle.setOnClickListener {
            favoriteChecker()
        }
    }

    /**
     * Checks if current weapon favorite icon, has to be activated or desactivated.
     * Also updates the object Weapon.favorite value.
     */
    fun favoriteChecker(){
        if (currentWeapon.favorite){
            favoriteToggle.setImageResource(R.drawable.favorit_empty)
            currentWeapon.favorite = false
            CoroutineScope(Dispatchers.IO).launch {
                val weapon = WeaponEntity(currentWeapon.uuid)
                WeaponApplication.database.weaponDao().deleteWeapon(weapon)
            }
        } else {
            favoriteToggle.setImageResource(R.drawable.favorit)
            currentWeapon.favorite = true
            CoroutineScope(Dispatchers.IO).launch {
                val weapon = WeaponEntity(currentWeapon.uuid)
                WeaponApplication.database.weaponDao().addWeapons(weapon)
            }
        }
    }
}
