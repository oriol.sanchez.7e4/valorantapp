package com.example.valorantapp.ui.main.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapp.R
import com.example.valorantapp.data.model.Data
import com.example.valorantapp.database.WeaponApplication
import com.example.valorantapp.ui.main.adapter.FavoritesAdapter
import com.example.valorantapp.ui.main.viewModel.WeaponsViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class FavoritesFragment: Fragment(R.layout.fragment_weapons) {
    private lateinit var recyclerView: RecyclerView
    private val viewModel: WeaponsViewModel by activityViewModels()
    private lateinit var favorite: ImageButton



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = FavoritesAdapter(viewModel)
        recyclerView = view.findViewById(R.id.recycler_view)
        favorite = view.findViewById(R.id.favoritesLink)

        viewModel.getWeapons()

        recyclerView.layoutManager = GridLayoutManager(context, 2)
        recyclerView.adapter = adapter

        CoroutineScope(Dispatchers.Main).launch {
            val weaponList = withContext(Dispatchers.IO) { WeaponApplication.database.weaponDao().getAllWeapons() }
            adapter.setFavoriteList(weaponList)
        }

        val data = Data(viewModel.favorites)

        adapter.setFavList(data)

        favorite.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.weaponsFragment)
        }
    }

}