package com.example.valorantapp.ui.main.view

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.valorantapp.R
import com.example.valorantapp.data.model.Weapon
import com.example.valorantapp.database.Database
import com.example.valorantapp.database.WeaponApplication
import com.example.valorantapp.database.dao.WeaponDao
import com.example.valorantapp.ui.main.viewModel.WeaponsViewModel
import com.example.valorantapp.ui.main.adapter.WeaponsAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class WeaponsFragment: Fragment(R.layout.fragment_weapons) {
    private lateinit var recyclerView: RecyclerView
    private val viewModel: WeaponsViewModel by activityViewModels()
    private lateinit var favorite: ImageButton



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycler_view)
        favorite = view.findViewById(R.id.favoritesLink)

        viewModel.getWeapons()
        val adapter = WeaponsAdapter(viewModel)

        recyclerView.layoutManager = GridLayoutManager(context, 2)
        recyclerView.adapter = adapter

        viewModel.weapons.observe(viewLifecycleOwner, {
            adapter.setWeaponList(it)
        })

        CoroutineScope(Dispatchers.Main).launch {
            val weaponList = withContext(Dispatchers.IO) { WeaponApplication.database.weaponDao().getAllWeapons() }
            adapter.setFavoriteList(weaponList)
        }

        favorite.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.favoritesFragment)
        }

    }

}