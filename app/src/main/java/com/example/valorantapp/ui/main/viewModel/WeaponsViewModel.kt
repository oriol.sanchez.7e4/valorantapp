package com.example.valorantapp.ui.main.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.valorantapp.data.model.Data
import com.example.valorantapp.data.model.Weapon
import com.example.valorantapp.data.repository.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeaponsViewModel: ViewModel() {
    private val repository = Repository()
    var weapons = MutableLiveData<Data>()
    var favorites = mutableListOf<Weapon>()
    var favoritesTabList = MutableLiveData<Weapon>()
    lateinit var weaponDetailed : Weapon

    init {
        loadWeapons()
    }

    /**
     * Gets the return of the API, and puts it in: weapons = MuteableLiveData<Data>
     */
    fun loadWeapons(){
        val call = repository.getWeapons()
        call.enqueue(object: Callback<Data>{
            override fun onResponse(
                call: Call<Data>,
                response: Response<Data>
            ) {
                weapons.postValue(response.body())
            }

            override fun onFailure(call: Call<Data>, t: Throwable) {
            }

        })
    }

    @JvmName("getWeapons1")
    fun getWeapons() = weapons

}
